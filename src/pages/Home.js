import { useEffect, useState } from 'react';
import DataTable from 'components/DataTable';
import axios from 'axios';
import {Add, Edit, Delete} from '@mui/icons-material';
import {Button, Stack, Box, Modal, Typography, TextField, Grid} from '@mui/material';

const Home = () => {

    const [data, setData] = useState([]);
    const [selected, setSelected] = useState([]);
    const [createOpen, setCreateOpen] = useState(false);
    const [updateOpen, setUpdateOpen] = useState(false);
    const [workCode, setWorkCode] = useState('');
    const [workName, setWorkName] = useState('');
    const [company, setCompany] = useState('');
    const [empName, setEmpName] = useState('');
    const [position, setPosition] = useState('');
    const [task, setTask] = useState('');
    const [telephone, setTelephone] = useState('');

    const handleCreateOpen = () => setCreateOpen(true);
    const handleCreateClose = () => setCreateOpen(false);
    const handleUpdateOpen = () => setUpdateOpen(true);
    const handleUpdateClose = () => setUpdateOpen(false);

    useEffect(() => {
        getData();
    }, []);

    useEffect(() => {
        console.log(selected);
    }, [selected]);

    const getData = () => {
        axios.get('/api')
            .then((res) => {
                setData(res.data);
        })
    }

    const createData = () => {
        axios.post(`/api`, {
            workCode: workCode,
            workName: workName,
            company: company,
            empName: empName,
            position: position,
            task: task,
            telephone: telephone,
        }).then(() => {
                getData();
        });
    }

    const updateData = (id) => {
        axios.put(`/api/${id}`, {
            workCode: workCode,
            workName: workName,
            company: company,
            empName: empName,
            position: position,
            task: task,
            telephone: telephone,
        }).then(() => {
                getData();
        })
    }

    const deleteData = (id) => {
        axios.delete(`/api/${id}`)
            .then(() => {
                getData();
        });
    }

    const handleCreateData = () => {
        console.log('onClickCreate');
        createData();
        handleCreateClose();
    }

    const handleUpdateData = () => {
        console.log('onClickUpdate');
        updateData(selected);
        handleUpdateClose();
    }

    const handleDeleteData = () => {
        console.log('onClickDelete');
        selected.map((id) => {
            deleteData(id);
        })
        setSelected([]);
    }

    const checkUpdatable = () => {
        if (selected.length == 1) {
            const editData = data.filter(el => el.empId == selected);
            setWorkCode(editData[0].workCode);
            setWorkName(editData[0].workName);
            setCompany(editData[0].company);
            setEmpName(editData[0].empName);
            setPosition(editData[0].position);
            setTask(editData[0].task);
            setTelephone(editData[0].telephone);

            handleUpdateOpen();
        }
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '6px solid #2196f3',
        borderRadius: '40px',
        boxShadow: 24,
        p: 8,
        alignItems: 'center'
      };

    return (
        <>
            <h1>사원 목록</h1>
            <Grid container justifyContent="center">
                <Stack direction="row" spacing={3} >
                    <Button variant="contained" startIcon={<Add />} onClick={handleCreateOpen}>
                        추가
                    </Button>
                    <Button variant="outlined" startIcon={<Edit />} onClick={checkUpdatable}>
                        수정
                    </Button>
                    <Button variant="outlined" color="error" startIcon={<Delete />} onClick={handleDeleteData}>
                        삭제
                    </Button>
                </Stack>
            </Grid>
            <br/>
            <DataTable setSelected={setSelected} data={data}/>

            {/* 생성 모달 */}
            <Modal
                open={createOpen}
                onClose={handleCreateClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} component="form" noValidate autoComplete='off'>
                    <Grid container justifyContent="center">
                        <Typography id="modal-modal-title" component="h2" variant="h5">사원 정보 생성</Typography>
                    </Grid>
                    <br/>
                    <Stack spacing={1}>
                        <TextField required id="outlined-required" label="업무코드" onChange={e => { setWorkCode(e.target.value) }}/>
                        <TextField required id="outlined-required" label="업무명" onChange={e => { setWorkName(e.target.value) }} />
                        <TextField required id="outlined-required" label="회사" onChange={e => { setCompany(e.target.value) }} />
                        <TextField required id="outlined-required" label="담당자" onChange={e => { setEmpName(e.target.value) }}/>
                        <TextField required id="outlined-required" label="직급" onChange={e => { setPosition(e.target.value) }} />
                        <TextField id="outlined-multiline-flexible" label="종류" multiline maxRows={4} onChange={e => { setTask(e.target.value) }}/>
                        <TextField required id="outlined-required" label="전화번호" onChange={e => { setTelephone(e.target.value) }}/>
                    </Stack>
                    <br/>
                    <Grid container justifyContent="flex-end">
                        <Stack spacing={2} direction="row">
                            <Button variant="contained" onClick={handleCreateData}>생성</Button>
                            <Button variant="outlined" onClick={handleCreateClose}>취소</Button>
                        </Stack>
                    </Grid>
                </Box>
            </Modal>

            {/* 수정 모달 */}
            <Modal
                open={updateOpen}
                onClose={handleUpdateClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} component="form" noValidate autoComplete='off'>
                    <Grid container justifyContent="center">
                        <Typography id="modal-modal-title" component="h2" variant="h5">사원 정보 수정</Typography>
                    </Grid>
                    <br/>
                    <Stack spacing={1}>
                        <TextField required id="outlined-required" label="업무코드" onChange={e => { setWorkCode(e.target.value) }} defaultValue={workCode} />
                        <TextField required id="outlined-required" label="업무명" onChange={e => { setWorkName(e.target.value) }} defaultValue={workName}/>
                        <TextField required id="outlined-required" label="회사" onChange={e => { setCompany(e.target.value) }} defaultValue={company}/>
                        <TextField required id="outlined-required" label="담당자" onChange={e => { setEmpName(e.target.value) }} defaultValue={empName}/>
                        <TextField required id="outlined-required" label="직급" onChange={e => { setPosition(e.target.value) }} defaultValue={position}/>
                        <TextField id="outlined-multiline-flexible" label="종류" multiline maxRows={4} onChange={e => { setTask(e.target.value) }} defaultValue={task}/>
                        <TextField required id="outlined-required" label="전화번호" onChange={e => { setTelephone(e.target.value) }} defaultValue={telephone}/>
                    </Stack>
                    <br/>
                    <Grid container justifyContent="flex-end">
                        <Stack spacing={2} direction="row">
                            <Button variant="contained" onClick={handleUpdateData}>수정</Button>
                            <Button variant="outlined" onClick={handleUpdateClose}>취소</Button>
                        </Stack>
                    </Grid>
                </Box>
            </Modal>
        </>
    );
}

export default Home;