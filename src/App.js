import {useEffect, useState} from 'react';
import axios from 'axios';
import Home from "pages/Home";

function App() {
  const [message, setMessage] = useState([]);

  useEffect(() => {
    axios({
      url: '/api/employees',
      method: 'GET'
    }).then((res) => {
      setMessage(res.data);
    })
  }, []);

  return (
    <div align="center">
      {/* <header className="App-header">
        <h1>1조_이승현</h1>
        <h2>직원 목록</h2>
        <table>
          <thead style={{background:'#FFE16F', color:'#282C34'}}>
            <tr>
              <td>이름</td>
              <td>회사1</td>
              <td>회사2</td>
              <td>직급</td>
              <td>종류</td>
              <td>전화번호</td>
            </tr>
          </thead>
          <tbody>
          { 
            message.map((employee, index) =>
              <tr key={index}>
                <td>{employee.name}</td>
                <td>{employee.company1}</td>
                <td>{employee.company2}</td>
                <td>{employee.position}</td>
                <td>{employee.task}</td>
                <td>{employee.telephone}</td>
              </tr>
            ) 
          }
          </tbody>
        </table>
      </header> */}

      <Home/>
    </div>
  );
}

export default App;
